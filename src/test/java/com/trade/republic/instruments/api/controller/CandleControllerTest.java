/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.instruments.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trade.republic.instruments.api.dto.CandleDto;
import com.trade.republic.instruments.api.persistence.model.Candle;
import com.trade.republic.instruments.api.persistence.model.CandleId;
import com.trade.republic.instruments.api.persistence.repository.CandleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.LinkedList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Class responsible of handling test cases for candles operations:
 *
 * <ul>
 *  <li> retrieves all candles generated in last 30 minutes
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RunWith(SpringRunner.class)
@WebMvcTest(CandleController.class)
public class CandleControllerTest {

    /**
     * Mock of candle repository.
     */
    @MockBean
    private CandleRepository candleRepository;

    /**
     * Mock of MVC to perform HTTP requests.
     */
    @Autowired
    private MockMvc mvc;

    /**
     * Object Mapper used to write JSON response data into content bodies.
     */
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void given_get_request_return_candles_last_30_minutes() throws Exception {

        final CandleId candleId = new CandleId();
        final Candle candle = new Candle(candleId, 1, 1.0, 1.5, 0.8, 1.2);

        final List<Candle> mockCandles = new LinkedList<>();
        mockCandles.add(candle);

        final CandleDto candleDto = new CandleDto(candle.getIsin(),
                candle.getCloseTime(),
                candle.getOpenTime(),
                candle.getOpenPrice(),
                candle.getHighPrice(),
                candle.getLowPrice(),
                candle.getClosePrice());

        final List<CandleDto> mockResponse = new LinkedList<>();
        mockResponse.add(candleDto);

        final String expectedResponse = objectMapper.writeValueAsString(mockResponse);

        given(candleRepository.findAllByCloseTimeBetween(any(), any()))
                .willReturn(mockCandles);

        mvc.perform(get("/candles"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

}
