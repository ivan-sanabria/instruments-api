/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.instruments.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trade.republic.instruments.api.redis.model.Instrument;
import com.trade.republic.instruments.api.redis.repository.InstrumentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Class responsible of handling test cases for instruments operations:
 *
 * <ul>
 *  <li> retrieves all the instruments stored in the cache
 *  <li> retrieves the instruments stored in the cache
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RunWith(SpringRunner.class)
@WebMvcTest(InstrumentController.class)
public class InstrumentControllerTest {

    /**
     * Mock of instrument repository.
     */
    @MockBean
    private InstrumentRepository instrumentRepository;

    /**
     * Mock of MVC to perform HTTP requests.
     */
    @Autowired
    private MockMvc mvc;

    /**
     * Object Mapper used to write JSON response data into content bodies.
     */
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void given_get_request_return_all_instruments() throws Exception {

        final List<Instrument> mockInstruments = new LinkedList<>();

        mockInstruments.add(new Instrument("d1", "isin-1", null));
        mockInstruments.add(new Instrument("d2", "isin-2", null));
        mockInstruments.add(new Instrument("d3", "isin-3", null));

        final String expectedResponse = objectMapper.writeValueAsString(mockInstruments);

        given(instrumentRepository.findAll())
                .willReturn(mockInstruments);

        mvc.perform(get("/instruments"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

    @Test
    public void given_invalid_isin_on_request_return_not_found() throws Exception {

        final String invalidIsin = "---invalid---";

        given(instrumentRepository.findById(invalidIsin))
                .willReturn(Optional.empty());

        mvc.perform(get("/instruments/".concat(invalidIsin)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void given_valid_isin_on_request_return_instrument() throws Exception {

        final String validIsin = "isin-2";
        final Instrument foundInstrument = new Instrument("d2", "isin-2", null);

        final String expectedResponse = objectMapper.writeValueAsString(foundInstrument);

        given(instrumentRepository.findById(validIsin))
                .willReturn(Optional.of(foundInstrument));

        mvc.perform(get("/instruments/".concat(validIsin)))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

}
