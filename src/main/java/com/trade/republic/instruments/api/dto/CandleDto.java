/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.instruments.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class to encapsulate the candle data supported by the instruments endpoints.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Candle data transfer object used to expose data to clients.", name = "Candle")
public class CandleDto {

    /**
     * International Securities Identification Number.
     */
    @Schema(description = "International Securities Identification Number.",
            example = "TI0774053X34")
    private String isin;

    /**
     * Close timestamp of the candle. This is unix time (UTC).
     */
    @Schema(description = "Close timestamp of the candle in unix time (UTC).",
            example = "1637276034")
    private Integer closeTime;

    /**
     * Open time of the candle. This is unix time (UTC).
     */
    @Schema(description = "Open timestamp of the candle in unix time (UTC).",
            example = "1638481381")
    private Integer openTime;

    /**
     * Open price of the instrument in specific minute.
     */
    @Schema(description = "Open price of the instrument in specific minute.",
            example = "10.26")
    private Double openPrice;

    /**
     * High price of the instrument in specific minute.
     */
    @Schema(description = "High price of the instrument in specific minute.",
            example = "12.49")
    private Double highPrice;

    /**
     * Low price of the instrument in specific minute.
     */
    @Schema(description = "Low price of the instrument in specific minute.",
            example = "10.01")
    private Double lowPrice;

    /**
     * Close price of the instrument in specific minute.
     */
    @Schema(description = "Close price of the instrument in specific minute.",
            example = "11.87")
    private Double closePrice;

}
