/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.instruments.api.controller;


import com.trade.republic.instruments.api.dto.HeartBeatDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class responsible of handling request made to / endpoint supporting:
 *
 * <ul>
 *  <li> heartbeat of the application for container monitoring
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RestController
public class HeartBeatController {

    /**
     * Version of the application given on pom.xml.
     */
    @Value("${application.version}")
    private String version;

    /**
     * Method to handle get requests made to / endpoint.
     *
     * @return Response entity with the heart beat response.
     */
    @GetMapping(value = "/", produces = "application/json")
    public ResponseEntity<HeartBeatDto> getHeartBeatResponse() {

        final String status = "UP";
        final HeartBeatDto heartBeatDto = new HeartBeatDto(version, status);

        return new ResponseEntity<>(heartBeatDto, HttpStatus.OK);
    }

}
