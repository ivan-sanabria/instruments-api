/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.instruments.api.persistence.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

/**
 * Class to encapsulate the data stored in DynamoDB.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBTable(tableName = "trade-republic-candle")
public class Candle {

    /**
     * Compose identifier that contains hash and range keys.
     */
    @Id
    @DynamoDBIgnore
    private CandleId id;

    /**
     * Open unix timestamp in UTC of the candle.
     */
    @DynamoDBAttribute
    private Integer openTime;

    /**
     * Open price of the instrument in specific minute.
     */
    @DynamoDBAttribute
    private Double openPrice;

    /**
     * High price of the instrument in specific minute.
     */
    @DynamoDBAttribute
    private Double highPrice;

    /**
     * Low price of the instrument in specific minute.
     */
    @DynamoDBAttribute
    private Double lowPrice;

    /**
     * Close price of the instrument in specific minute.
     */
    @DynamoDBAttribute
    private Double closePrice;

    /**
     * Retrieves the international securities identification number of the compose id.
     *
     * @return String representing international securities identification number.
     */
    @DynamoDBHashKey(attributeName = "isin")
    public String getIsin() {
        return (null != id) ? id.getIsin() : null;
    }

    /**
     * Set the international securities identification number of the compose id.
     *
     * @param isin International Securities Identification Number.
     */
    public void setIsin(String isin) {
        if (null == id)
            id = new CandleId();

        id.setIsin(isin);
    }

    /**
     * Retrieves the close time of the compose id.
     *
     * @return Number representing the close time of the candle.
     */
    @DynamoDBRangeKey(attributeName = "closeTime")
    public Integer getCloseTime() {
        return (null != id) ? id.getCloseTime() : null;
    }

    /**
     * Set the close time of the compose id.
     *
     * @param closeTime Close time of the candle.
     */
    public void setCloseTime(Integer closeTime) {
        if (null == id)
            id = new CandleId();

        id.setCloseTime(closeTime);
    }

}
