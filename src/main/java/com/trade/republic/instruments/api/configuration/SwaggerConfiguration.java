/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.instruments.api.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Class responsible of swagger configuration.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Configuration
public class SwaggerConfiguration {

    /**
     * Version of the application given on pom.xml.
     */
    @Value("${application.version}")
    private String version;

    /**
     * Build the grouped open api instance used by swagger to build api documentation.
     *
     * @return Grouped open api instance used to build api documentation.
     */
    @Bean
    public GroupedOpenApi api() {

        return GroupedOpenApi.builder()
                .pathsToMatch("/instruments/**", "/candles/**")
                .group("Trade Republic Services")
                .build();
    }

    /**
     * Build open api info instance used by swagger to display it on main api documentation page.
     *
     * @return Open api instance used to build api documentation.
     */
    @Bean
    public OpenAPI apiEndPointsInfo() {

        return new OpenAPI()
                .info(new Info()
                        .title("Instrument Services")
                        .description("Service responsible of exposing instruments and candle data to clients.")
                        .contact(new Contact()
                                .name("Iván Camilo Sanabria").email("icsanabriar@googlemail.com")
                                .url("https://www.linkedin.com/in/icsanabriar/en"))
                        .license(new License()
                                .name("Apache 2.0")
                                .url("http://www.apache.org/licenses/LICENSE-2.0.html"))
                        .version(version));
    }

}
