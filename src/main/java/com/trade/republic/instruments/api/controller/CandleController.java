/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.instruments.api.controller;

import com.trade.republic.instruments.api.dto.CandleDto;
import com.trade.republic.instruments.api.persistence.repository.CandleRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class responsible of handling request made to /candles endpoint supporting:
 *
 * <ul>
 *  <li> retrieves all candles generated in last 30 minutes
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RestController
@Tag(name = "Candle Endpoints", description = "Supported operations to expose candle data to clients.")
public class CandleController {

    /**
     * Define minutes in time range to query candles.
     */
    private static final Integer RANGE = 30;

    /**
     * Define candle repository to retrieve data from dynamo db.
     */
    private final CandleRepository candleRepository;

    /**
     * Constructor of the instrument controller to inject required services.
     *
     * @param candleRepository Candle repository used to retrieve candle data.
     */
    @Autowired
    public CandleController(CandleRepository candleRepository) {
        this.candleRepository = candleRepository;
    }

    /**
     * Method to handle get request to /candles endpoint.
     *
     * @return Response entity with the list of candles data.
     */
    @Operation(summary = "Find all candles generated in the last 30 minutes based on UTC.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully found the candle data generated on the last 30 minutes.",
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = CandleDto.class)))),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the request.",
                    content = @Content)
    })
    @GetMapping(value = "/candles", produces = "application/json")
    public ResponseEntity<List<CandleDto>> getCandles() {

        final Instant currentTime = Instant.now(Clock.systemUTC())
                .truncatedTo(ChronoUnit.MINUTES);

        final Integer startTime = Math.toIntExact(
                currentTime.minus(RANGE, ChronoUnit.MINUTES)
                        .getEpochSecond());

        final Integer endTime = Math.toIntExact(
                currentTime.getEpochSecond());

        final List<CandleDto> candles = candleRepository.findAllByCloseTimeBetween(startTime, endTime)
                .stream()
                .map(candle ->
                        new CandleDto(
                                candle.getIsin(),
                                candle.getCloseTime(),
                                candle.getOpenTime(),
                                candle.getOpenPrice(),
                                candle.getHighPrice(),
                                candle.getLowPrice(),
                                candle.getClosePrice()))
                .collect(Collectors.toList());

        return new ResponseEntity<>(candles, HttpStatus.OK);
    }

}
