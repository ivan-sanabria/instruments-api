/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.instruments.api.persistence.repository;

import com.trade.republic.instruments.api.persistence.model.Candle;
import com.trade.republic.instruments.api.persistence.model.CandleId;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Class responsible of handling candles persistence against dynamo db.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@EnableScan
@EnableScanCount
@Repository
public interface CandleRepository extends PagingAndSortingRepository<Candle, CandleId> {

    /**
     * Find candles in specific time range.
     *
     * @param startTime Start time of the range.
     * @param endTime   End time of the range.
     * @return List of candles found in the given time range.
     */
    List<Candle> findAllByCloseTimeBetween(Integer startTime, Integer endTime);

}
