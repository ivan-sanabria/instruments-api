/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.instruments.api.persistence.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import lombok.Data;

import java.io.Serializable;

/**
 * Class to encapsulate the candle identifier persisted in dynamo db.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Data
public class CandleId implements Serializable {

    /**
     * International Securities Identification Number.
     */
    @DynamoDBHashKey
    private String isin;

    /**
     * Close timestamp of the candle. This is unix time in UTC.
     */
    @DynamoDBRangeKey
    private Integer closeTime;

}
