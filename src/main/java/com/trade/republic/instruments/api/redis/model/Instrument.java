/*
 * Copyright (C) 2021 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.trade.republic.instruments.api.redis.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

/**
 * Class to encapsulate the instrument data stored in redis.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Data
@AllArgsConstructor
@Schema(description = "Instrument data transfer object used to expose data to clients.")
@RedisHash("instruments")
@TypeAlias("instruments")
public class Instrument implements Serializable {

    /**
     * International Securities Identification Number.
     */
    @Id
    @Schema(description = "International Securities Identification Number.",
            example = "TI0774053X34")
    private String isin;

    /**
     * Description of the instrument.
     */
    @Schema(description = "Description of the instrument.",
            example = "auctor eruditi definitiones")
    private String description;

    /**
     * Last price of the instrument.
     */
    @Schema(description = "Last price of the instrument.",
            example = "20.306")
    private Double price;

}