# Instruments API - Interview Challenge

version 1.2.0 - 22/02/2023

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/instruments-api.svg)](http://bitbucket.org/ivan-sanabria/instruments-api/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/instruments-api.svg)](http://bitbucket.org/ivan-sanabria/instruments-api/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_instruments-api&metric=alert_status)](https://sonarcloud.io/project/overview?id=ivan-sanabria_instruments-api)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_instruments-api&metric=bugs)](https://sonarcloud.io/project/issues?resolved=false&types=BUG&id=ivan-sanabria_instruments-api)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_instruments-api&metric=coverage)](https://sonarcloud.io/component_measures?id=ivan-sanabria_instruments-api&metric=coverage)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_instruments-api&metric=ncloc)](https://sonarcloud.io/code?id=ivan-sanabria_instruments-api)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_instruments-api&metric=sqale_index)](https://sonarcloud.io/component_measures?metric=Maintainability&id=ivan-sanabria_instruments-api)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_instruments-api&metric=vulnerabilities)](https://sonarcloud.io/project/issues?resolved=false&types=VULNERABILITY&id=ivan-sanabria_instruments-api)

## Specifications

Please review the section **Instrument-Price retrieval** and **Aggregated-Price History retrieval** of the README file under specifications folder.

## Requirements

- JDK 11.x
- Maven 3.6.3
- IDE for JAVA (Eclipse, Netbeans, IntelliJ).
- Postman 10.10.x
- JMeter 5.1.x
- Redis Server 7.0.x
- [Docker 2.3.x](https://www.docker.com/products/docker-desktop)
- [EB CLI 3.18.x](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-install-advanced.html)

## Application Setup

To run the application using DynamoDB and ElastiCache as data sources:

1. Set environment variables **AWS_ACCESS_KEY_ID** and **AWS_SECRET_ACCESS_KEY** in your .bash_profile to have access to DynamoDB and ElastiCache.
2. Ensure that the given AWS credentials have the proper policies to read the data from both data sources.
3. Set environment variable **REDIS_HOST** in your .bash_profile as localhost or URL that your redis server is hosted.
4. Check the port used in your redis server and update the value at **application.yml**.

## Running Application inside IDE

To run the application on your IDE:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Integrate your project with IDE:
    - [Eclipse](http://books.sonatype.com/m2eclipse-book/reference/creating-sect-importing-projects.html)
    - [Netbeans](http://wiki.netbeans.org/MavenBestPractices)
    - [IntelliJ]( https://www.jetbrains.com/idea/help/importing-project-from-maven-model.html)
5. Open IDE and run the class **Application.java**.

## Running Application on Terminal

To run the application on terminal with Maven:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    java -jar target/instruments-api-1.2.0.jar
```

## Running Unit Tests on Terminal

To run the unit tests on terminal:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean test
```

## Running Application using Docker

To run the application on terminal with docker:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Verify the version of Docker - 19.x or higher.
4. Download the source code from repository.
5. Open a terminal.
6. Go to the root location of the source code.
7. Execute the commands:

```bash
    mvn clean package
    docker build --build-arg AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} --build-arg AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} --build-arg REDIS_HOST=host.docker.internal -t instruments-api .
    docker run -p 8081:8081 instruments-api
```

## Check Application Test Coverage using Jacoco

To verify test coverage on terminal:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    open target/site/jacoco/index.html
```

## Testing using Postman Collection

To test using the postman collection:

1. Run the application inside IDE or Terminal.
2. Open Postman application.
3. Import the postman collection **instruments-api-collection.json**.
4. Import the postman environment **instruments-api-environment.json**.
5. Check the environment value of **api_host** point to localhost and port your application is running.
6. Move the mouse over the collection.
7. Click on the three dots on the right side.
8. Click on **Run collection**.
9. Check the number of iterations is 1.
10. Click on **Run Instruments API** button.
11. Validate all tests passed.

## Load Test using JMeter

To load test the application using JMeter GUI:

1. Run the application inside IDE or Terminal.
2. Open JMeter GUI.
3. Open **instruments-api.jmx**.
4. Start the load test.

Be aware of the throughput of the application, it depends on the DynamoDB Read Capacity configuration on AWS.

## List Dependency Licenses

To generate list of dependency licenses on terminal:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn project-info-reports:dependencies
    open target/site/dependencies.html
```

## Continuous Deployment using Bitbucket Pipelines and Elastic Beanstalk

Deployment to production environment is already configured on **bitbucket-pipelines.yml**. This would create a new environment
on every deployment. 

In order to validate that whole process was make properly execute the following commands:
```bash
    curl -X GET -I http://${URL}/status
    curl -X GET -I http://${URL}/candles
    curl -X GET -I http://${URL}/instruments
    curl -X GET -I http://${URL}/instruments/LW01303F0611
```

To save cost on AWS it is recommended to terminate the environment after the exercise is covered:

```bash
    cd deployment
    eb terminate instruments-api-production
```

## API Documentation

To see swagger documentation:

1. Open a browser
2. Go to http://${URL}/swagger-ui.html

# Contact Information

Email: icsanabriar@googlemail.com