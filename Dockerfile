FROM adoptopenjdk/openjdk11:ubi

LABEL maintainer="icsanabriar@googlemail.com"

ARG AWS_ACCESS_KEY_ID=""
ENV AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID

ARG AWS_SECRET_ACCESS_KEY=""
ENV AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY

ARG REDIS_HOST=""
ENV REDIS_HOST=$REDIS_HOST

ARG JAR_FILE=target/instruments-api-*.jar
ADD ${JAR_FILE} instruments-api.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/instruments-api.jar"]

EXPOSE 8081 6379